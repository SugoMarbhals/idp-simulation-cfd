## LOGBOOK W5 (Get it fucking done)
| Event         | Platform      | 
| ------------- |-----------|
|  Agenda | -Discuss with Fiqri on the parameters needed for simulation <br/> - Help teammates on running simulation(Ansys) on the cad model|
|  Goals | - Perform a simulation for the HAU model|
|  Decisions | - Decided to use Ansys for simulation of HAU.|
|  Methods | - UseANSYS Fluent to simulate HAU |
| Justifications | - Must consider the skewness and orthogonal quality during meshing process <br/> -Simulation must be accurate as possible <br/> - To have a good results on Fluid Dynamics concept. |
|  Impacts | - Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes |
|  Next Step | - Run the simulation again to maintain consistency <br/> - Find the parameters needed using the reference books provided|

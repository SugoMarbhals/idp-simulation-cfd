### **Important to list out the problems first**

What are our Problems/Findings: 

Ansys is heavier on lower end hardware and thus consumes more time than usual, additionally the HAU must be inspected in real time.
The measurements of real time HAU taken are not accurate due to systematic error and random error.

| Event         | Platform      | 
| ------------- |-----------|
| 1. Agenda | - Run the simulation for 2D model for the airship <br/> - Helping each other run the simulation|
| 2. Goals | - run simulations for the 2D HAU model <br/> - plotting Cl vs angle of attack graph |
| 3. Decisions | - Use Ansys Fluent Flow to simulate HAU for Cl & Cd values <br/> - plotting Cl vs angle of attack|
| 4. Methods | - Use ANSYS Fluent to simulate the HAU <br/> - Excel is used to plot graphs|
|5. Justifications| - Must consider skewness and orthogonal qualtiy during meshing process <br/> - Refine and smoothen meshing around the airship |
| 6. Impacts | - Consider other suitable parameters that are relevant (Lift, Drag, Thrust, Weight) <br/> - to see results for the Cl vs angle of attack graph |
| 7. Next Steps | -Continue to obtain Cl and Cd values for more accurate readings |

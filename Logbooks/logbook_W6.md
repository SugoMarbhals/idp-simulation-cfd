### **NOTES AND DISCALIMERAS**

 ISSUES: 

A virtual inspection of the HAU itself is difficult, and thus we have to look for it in real time.
Furthermore, HAU measurements in real time are innaccurate due to systematic error and random error.

| Event         | Description      | 
| ------------- |-----------|
| 1. Agenda | - Run  simulation for 3D model for the airship <br/> - Helping other teammates run simulation|
| 2. Goals | - Run simulation for the HAU model <br/> - Acquire parameters needed from other groups|
| 3. Decisions | - Using simulator to simulate the HAU.|
| 4. Methods | - Using ANSYS Fluent/Student to simulate HAU behaviour |
|5. Justifications| - Must consider skewness and orthogonal qualtiy during meshing process <br/> - Properly perform simulation <br/> - Obtain good results on Fluid Dynamics concept. |
| 6. Impacts | - Think of  possible parameters involved (Lift, Drag, Thrust, Weight) <br/> - Possible CFD outcomes |
| 7. Next Steps | - discuss with Fiqri, what is the initial set up and condition needed|

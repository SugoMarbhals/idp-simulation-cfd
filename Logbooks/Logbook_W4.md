# Logbook W4

TARGETS       |    DESCRIPTIONS
--------------|---------------
AGENDA        |  -Selection pf the optimal HAU design <br/> -Designing HAU using CAD <br/> -Discussion of timeline
GOALS         | -To design HAU using CATIA using the parameters obtained from physical lab session
DECISIONS     | -Designing of HAU designs between team members to determine the most reliable and acceptable design
METHODS       | -By comparing HAU designs, we could decide which HAU was the best, and proceed using said design
JUSTIFICATION | -More designs means more data as each design was put through simulation using the same parameters
IMPACT        |  -Managed to get a general idea of what should be considered an optimal design for HAU
NEXT STEP     | -Calculations <br/> -Review of parameters involved from other subsystems

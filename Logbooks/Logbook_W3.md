# Logbook W3


Questions/Targets | Ideas
----------------- | -------------
Agenda            | -To form out the Group Progress Gantt Chart, discuss timeline regarding measurements, data processing, CATIA etc)       
Goals             | -Inspect HAU model design and take measurements)top, side bottom views)
Decisions         | -Visited Lab H.2.1 to inspect and observe HAU model         
Methods           | -Measurements taken have their tolerances set accordingly to minimise potential human/parallax errors
Justifications    | -To come up with a prototype which both looks good, and functions as intended, thus maintaining performance.
Impacts           | -CATIA design of HAU should be illustrated and planned out properly(rough sketches made for visualisation)  
Next Step         | Calculations and designing process       
